<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class TaskController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function getTasks()
    {
    	$tasks = Task::select(['id','name','category','state']);

    	return Datatables::of($tasks)->make(true);
    }
}
