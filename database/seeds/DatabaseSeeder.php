<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();

    	for($i = 0; $i < 500; $i++){
	      DB::table('tasks')->insert(array(
	      	'name'			=> $faker->firstName,
	      	'category'	=> $faker->lastName,
	      	'state'		=> $faker->city
	      ));
    	}

    }
}
