<!DOCTYPE html>
<html>
<head>
    <title>Laravel DataTables</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="/css/dataTables.bootstrap.min.css">
</head>
<body>

<div class="container">
    <table id="task" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Ciudad</th>
        </tr>
        </thead>
    </table>
</div>

<script src="/js/jquery-2.1.4.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        oTable = $('#task').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('datatable.tasks') }}",
            "columns": [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'category', name: 'category'},
                {data: 'state', name: 'state'}
            ]
        });
    });
</script>
</body>
</html>