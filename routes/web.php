<?php

Route::get('/', 'TaskController@index');

Route::get('/tasks', 'TaskController@getTasks')->name('datatable.tasks');